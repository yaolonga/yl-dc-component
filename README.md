# yl-dc-component

#### 描述

一个动态查询json生成组件，用于构建复杂的条件查询，一般用于配合后端数据库查询

#### 演示
![演示图片](img/yanshi.png)

#### 组件说明

##### 属性

- DynamicConditionProps

| 属性名             | 说明           | 类型                            | 默认值                                                                                         | 必传    | 备注 |
|-----------------|--------------|-------------------------------|---------------------------------------------------------------------------------------------|-------|----|
| size            | 尺寸           | "small" 、 "large" 、 "default" | 'small'                                                                                     | false |    |
| fromSchemas     | 字段配置         | FormSchema[]                  | []                                                                                          | true  |    |
| value (v-model) | 查询条件节点       | GroupNode[]                   |                                                                                             | true  |    |    
| storeOption     | 条件josn存储配置选项 | ConditionStorageOption        | {<br/>mode: 'localstorage',<br/>conf: { <br/>storageKey: 'CONDITION_AST_STORAGE_KEY'<br/>}} | false |    |
| layoutOption    | 布局选项         | {cols: number}                | {cols: 2}                                                                                   | false |    |

- FormSchema

| 属性名            | 说明   | 类型              | 默认值     | 必传    | 备注 |
|----------------|------|-----------------|---------|-------|----|
| label          | 标签   | string          | ''      | true  |    |
| field          | 字段名  | string          | ''      | true  |    |
| conditionTypes | 条件类型 | ConditionType[] | ['eq']  | false |    |
| component      | 组件类型 | ComponentType   | 'Input' | false |    |
| componentProps | 组件属性 | object          | {}      | false |    |
| defaultValue   | 默认值  | any             | ''      | false |    |

- ComponentType

```ts
type ComponentType =
    | 'Input'
    | 'InputGroup'
    | 'InputPassword'
    | 'InputSearch'
    | 'InputTextArea'
    | 'InputNumber'
    | 'InputCountDown'
    | 'Select'
    | 'ApiSelect'
    | 'TreeSelect'
    | 'ApiTreeSelect'
    | 'RadioButtonGroup'
    | 'RadioGroup'
    | 'Checkbox'
    | 'CheckboxGroup'
    | 'AutoComplete'
    | 'Cascader'
    | 'DatePicker'
    | 'MonthPicker'
    | 'RangePicker'
    | 'WeekPicker'
    | 'TimePicker'
    | 'Switch'
    | 'StrengthMeter'
    | 'Upload'
    | 'IconPicker'
    | 'Render'
    | 'Slider'
    | 'Rate'
    | 'Avatar'
    | 'Divider' | Component | (() => JSX.Element)
```

- ConditionStorageOption

| 属性名  | 说明   | 类型                          | 必传    | 备注 |
|------|------|-----------------------------|-------|----|
| mode | 存储模式 | 'localstorage' \| ‘api'     | false |    |
| conf | 配置项  | LocalStorageConf \| ApiConf | false |    |

- LocalStorageConf

| 属性名        | 说明    | 类型     | 必传   | 备注 |
|------------|-------|--------|------|----|
| storageKey | 存储key | string | true |    |

- ApiConf

| 属性名         | 说明           | 类型                                               | 必传   | 备注                    |
|-------------|--------------|--------------------------------------------------|------|-----------------------|
| getAllApi   | 获取所有条件AST接口  | () => Promise<DynamicConditionAST[]>             | true |                       |
| getApi      | 根据key获取条件AST | (key: string) => Promise<DynamicConditionAST>    | true |                       |
| updateApi   | 更新条件AST      | (condition: DynamicConditionAST) => Promise<any> | true |                       |
| saveApi     | 保存条件AST      | (condition: DynamicConditionAST) => Promise<any> | true |                       |
| delApi      | 删除条件AST      | (key: string) => Promise<any>                    | true |                       |
| resultField | api请求结果字段    | string                                           | true | api请求结果字段 支持 xx.xx.xx |

- DynamicConditionAST （该类型为json ast）

| 属性名    | 说明       | 类型          | 备注 |
|--------|----------|-------------|----|
| key    | ast唯一key | string      |    |    |
| name   | 名称       | string      |    |    |
| groups | 条件组      | GroupNode[] |    |    |

- GroupNode

| 属性名        | 说明   | 类型              | 备注 |
|------------|------|-----------------|----|
| logicType  | 逻辑类型 | LogicType       |    | |
| conditions | 条件节点 | ConditionNode[] |    | |

- ConditionNode

| 属性名       | 说明   | 类型            | 备注 |
|-----------|------|---------------|----|
| logicType | 逻辑类型 | LogicType     |    | |
| type      | 条件类型 | ConditionType |    | |
| field     | 条件字段 | string        |    | |
| value     | 条件值  | any           |    | |

- LogicType

| 类型  | 说明 | 备注 |
|-----|----|----|
| and | 与  |    |
| or  | 或  |    |

- ConditionType

| 类型           | 说明   | 备注 |
|--------------|------|----|
| eq           | 等于   |    |
| not_eq       | 不等于  |    |
| greater_than | 大于   |    |
| less_than    | 小于   |    |
| like         | 模糊匹配 |    |
| in           | 包含   |    |
| not_in       | 不包含  |    |
| null         | 空值   |    |
| not_null     | 非空值  |    |

##### 事件

| 事件名                      | 说明       | 参数 |
|--------------------------|----------|----|
| search                   | 搜索事件     | 无  |
| searchConditionSelect    | 搜索条件选择事件 | 无  |
| searchConditionSelectDel | 搜索条件删除事件 | 无  |
| reset                    | 重置事件     | 无  |
| save                     | 条件保存事件   | 无  |

#### 使用

##### 安装

```.sybase
yarn add yl-dc-component
npm install yl-dc-component
```

##### 引入

```.js
import 'yl-dc-component/dist/style.css'
import ylDcComponent from 'yl-dc-component'
```

#### 示例

```.vue
<script setup lang="ts">
import 'yl-dc-component/dist/style.css'
import {DynamicCondition} from 'yl-dc-component'
import {FormSchema} from "yl-dc-component/dist/components/DynamicCondition/src/types/schemas";
import {GroupNode} from "yl-dc-component/dist/components/DynamicCondition/src/types/node";
import {ref} from "vue";

const schemas: FormSchema[] = [{
  label: "配置名称",
  defaultValue: "测试",
  conditionTypes: ['eq', 'like'],
  field: "name"
}, {
  label: "接入类型",
  defaultValue: "MicroId_IOT111111",
  conditionTypes: ["not_in", "in", 'eq'],
  component: 'Select',
  componentProps: {
    placeholder: '请输入',
    options: [{
      label: '微标Iot平台',
      value: 'MicroId_IOT'
    }, {
      label: '阿里云Iot平台',
      value: 'Aliyun_Iot'
    }]
  },
  field: "type"
}];

function reset() {
  console.log('reset')
}

async function search(ast: GroupNode[]) {
  console.log('search', ast)
  const res = await getDataSource({
    pageSize: 10,
    page: 1,
    ast
  })

  console.log("搜索结果：", res)
}

function save() {
  console.log('save')
}


type SearchModel = {
  pageSize: number;
  page: number;
  ast: GroupNode[]
}

async function getDataSource(searchModel: SearchModel) {
  return fetch("http://localhost:8080/api/page", {
    method: "POST",
    body: JSON.stringify(searchModel) as any,
    headers: {
      "Content-Type": 'application/json'
    }
  })
}

const value = ref<GroupNode[]>();

</script>

<template>
  <div style="background: #eeeeee;height: 100vh">
    <dynamic-condition
        @reset="reset"
        @search="search"
        @save="save"
        v-model:value="value"
        size="small"
        :fromSchemas="schemas">
    </dynamic-condition>
  </div>
</template>

<style scoped>

</style>
```