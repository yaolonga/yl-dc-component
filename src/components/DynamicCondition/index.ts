import _DynamicCondition from './src/DynamicCondition';
import { dynamicConditionProps } from './src/props';
import { withInstall } from '/@/utils/withInstall';
import { ExtractPropTypes } from 'vue';

export * from './src/types/schemas';
export * from './src/types/node';

export const DynamicCondition = withInstall(_DynamicCondition);

export declare type DynamicConditionProps = Partial<ExtractPropTypes<typeof dynamicConditionProps>>;

export default DynamicCondition;
