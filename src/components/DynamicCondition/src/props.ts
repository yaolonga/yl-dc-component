import { PropType } from 'vue';
import { GroupNode } from './types/node';
import { DynamicConditionProps } from './types/schemas';

type LayoutOption = DynamicConditionProps['layoutOption'];
type StoreOption = DynamicConditionProps['storeOption'];
type SizeType = DynamicConditionProps['size'];
type FormSchemas = DynamicConditionProps['formSchemas'];

export const dynamicConditionProps = {
  value: {
    type: Object as PropType<GroupNode[]>,
    default: () => [],
  },
  fromSchemas: {
    type: Array as PropType<FormSchemas>,
    required: true,
  },
  layoutOption: {
    type: Object as PropType<LayoutOption>,
    default: () =>
      ({
        cols: 2,
      } as LayoutOption),
  },
  size: {
    type: String as PropType<SizeType>,
    default: () => 'small',
  },
  showMoreButton: {
    type: Boolean,
    default: true,
  },
  storeOption: {
    type: Object as PropType<StoreOption>,
    default: () =>
      ({
        mode: 'localstorage',
        conf: {
          storageKey: 'CONDITION_AST_STORAGE_KEY',
        },
      } as StoreOption),
  },
};
