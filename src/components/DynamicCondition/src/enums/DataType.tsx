import type { DataType } from '../types/node';

type EnumValue<T> = {
  label: string;
  value: T;
};
type DataTypeEnumValue = {
  renderComponent?: () => JSX.Element;
} & EnumValue<DataType>;

type DataTypeEnum = {
  [key in DataType]: DataTypeEnumValue;
};

export const DATA_TYPE: DataTypeEnum = {
  INTEGER: {
    label: '整型',
    value: 'INTEGER',
  },
  SHORT: {
    label: '短整',
    value: 'SHORT',
  },
  LONG: {
    label: '长整',
    value: 'LONG',
  },
  STRING: {
    label: '字符串',
    value: 'STRING',
  },
  DOUBLE: {
    label: '双精度浮点',
    value: 'DOUBLE',
  },
  FLOAT: {
    label: '单精度浮点',
    value: 'FLOAT',
  },
  DATE: {
    label: '日期',
    value: 'DATE',
  },
  DATE_TIME: {
    label: '日期（时分秒）',
    value: 'DATE_TIME',
  },
  OBJECT: {
    label: '对象',
    value: 'OBJECT',
  },
  ENUM: {
    label: '枚举',
    value: 'ENUM',
  },
  ARRAY: {
    label: '数组',
    value: 'ARRAY',
  },
  BOOLEAN: {
    label: '布尔',
    value: 'BOOLEAN',
  },
};
