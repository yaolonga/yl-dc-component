import type { ConditionType } from '../types/node';

type ConditionTypeValue<T> = {
  label: string;
  value: T;
};
type ConditionTypeEnumValue = ConditionTypeValue<ConditionType>;

type ConditionTypeEnum = {
  [key in ConditionType]: ConditionTypeEnumValue;
};

export const CONDITION_TYPE: ConditionTypeEnum = {
  greater_than: {
    label: '>',
    value: 'greater_than',
  },
  less_than: {
    label: '<',
    value: 'less_than',
  },
  eq: {
    label: '=',
    value: 'eq',
  },
  not_eq: {
    label: '!=',
    value: 'not_eq',
  },
  in: {
    label: '在...中',
    value: 'in',
  },
  not_in: {
    label: '不在...中',
    value: 'not_in',
  },
  like: {
    label: '模糊',
    value: 'like',
  },
  null: {
    label: '为空',
    value: 'null',
  },
  notNull: {
    label: '不为空',
    value: 'notNull',
  },
};
