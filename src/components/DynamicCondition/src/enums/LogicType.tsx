import type { LogicType } from '../types/node';

type LogicTypeValue<T> = {
  label: string;
  value: T;
};
type LogicTypeEnumValue = LogicTypeValue<LogicType>;

type LogicTypeEnum = {
  [key in LogicType]: LogicTypeEnumValue;
};

export const LOGIC_TYPE: LogicTypeEnum = {
  or: {
    label: '或者',
    value: 'or',
  },
  and: {
    label: '并且',
    value: 'and',
  },
};
