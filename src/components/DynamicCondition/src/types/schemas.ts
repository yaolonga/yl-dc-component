import { ConditionType, DynamicConditionAST } from './node';
import { ConditionStorageOption } from '../store';
import { Component } from 'vue';

export interface FormSchema {
  field: string;
  label: string;
  defaultValue?: any;
  // 支持的条件类型
  conditionTypes?: ConditionType[];
  component?: ComponentType;
  componentProps?: Record<string, any>;
}

export interface RenderCallbackParams {
  schema: FormSchema;
  values: Record<string, any>;
  model: Record<string, any>;
  field: string;
}

export interface DynamicConditionProps {
  // 条件AST
  conditions?: DynamicConditionAST;
  // 渲染搜索表单schema
  formSchemas: FormSchema[];
  // 大小
  size?: 'small' | 'large' | 'default';
  // 是否显示筛选更多按钮
  showMoreButton?: boolean;
  // 条件存储方式 本地、远程api
  storeOption?: ConditionStorageOption;
  // 布局选项
  layoutOption?: {
    cols: number;
  };
}

export interface DynamicConditionActionType {
  // 搜索
  search: () => Promise<void>;
  // 保存
  save: () => Promise<void>;
  setFieldsValue: <T>(values: T) => Promise<void>;
  resetFields: () => Promise<void>;
  getFieldsValue: () => Record<string, any>;
  clearValidate: (name?: string | string[]) => Promise<void>;
  updateSchema: (data: Partial<FormSchema> | Partial<FormSchema>[]) => Promise<void>;
  resetSchema: (data: Partial<FormSchema> | Partial<FormSchema>[]) => Promise<void>;
  setProps: (props: Partial<DynamicConditionProps>) => Promise<void>;
  removeSchemaByFiled: (field: string | string[]) => Promise<void>;
  appendSchemaByField: (
    schema: FormSchema,
    prefixField: string | undefined,
    first?: boolean | undefined,
  ) => Promise<void>;
}

export type RegisterFn = (formInstance: DynamicConditionActionType) => void;

export type ComponentType =
  | 'Input'
  | 'InputGroup'
  | 'InputPassword'
  | 'InputSearch'
  | 'InputTextArea'
  | 'InputNumber'
  | 'InputCountDown'
  | 'Select'
  | 'ApiSelect'
  | 'TreeSelect'
  | 'ApiTreeSelect'
  | 'RadioButtonGroup'
  | 'RadioGroup'
  | 'Checkbox'
  | 'CheckboxGroup'
  | 'AutoComplete'
  | 'Cascader'
  | 'DatePicker'
  | 'MonthPicker'
  | 'RangePicker'
  | 'WeekPicker'
  | 'TimePicker'
  | 'Switch'
  | 'StrengthMeter'
  | 'Upload'
  | 'IconPicker'
  | 'Render'
  | 'Slider'
  | 'Rate'
  | 'Avatar'
  | 'Divider'
  | Component
  | (() => JSX.Element);
