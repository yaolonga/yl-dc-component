export type DataType =
  | 'INTEGER'
  | 'SHORT'
  | 'LONG'
  | 'STRING'
  | 'DOUBLE'
  | 'FLOAT'
  | 'DATE'
  | 'DATE_TIME'
  | 'OBJECT'
  | 'ENUM'
  | 'ARRAY'
  | 'BOOLEAN';
export type LogicType = 'or' | 'and';
export type ConditionType =
  | 'greater_than'
  | 'less_than'
  | 'eq'
  | 'not_eq'
  | 'in'
  | 'not_in'
  | 'like'
  | 'null'
  | 'notNull';
export type NodeType = 'ConditionNode' | 'GroupNode';

export interface Node {
  logicType: LogicType;
  nodeType: NodeType;
}

export interface ConditionNode extends Node {
  type: ConditionType;
  // valueType: {
  //     type: DataType
  //     [key: string]: any
  // };
  field: string;
  value: any;
}

export interface GroupNode extends Node {
  conditions: ConditionNode[];
}

export type DynamicConditionAST = {
  key: string; // ast唯一key
  name: string; // 名称
  groups: GroupNode[];
};
