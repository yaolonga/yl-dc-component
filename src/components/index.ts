import './style.css'
import "ant-design-vue/dist/antd.css";
import {DynamicCondition, type DynamicConditionProps} from "./DynamicCondition"

export type {
    DynamicConditionProps
}

export {
    DynamicCondition
}


declare module 'vue' {
    export interface GlobalComponents {
        YlDynamicCondition: typeof DynamicCondition;
    }
}
